import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

class ToastUtils {
  static void showToast(BuildContext context, String msg) {
    if(context != null) {
      Flushbar(
        message: msg,
        duration: Duration(seconds: 2),
      )
        ..show(context);
    }
  }

}