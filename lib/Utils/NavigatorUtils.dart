import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myluck/Screens/InfoScreens/AboutAppScreen.dart';
import 'package:myluck/Screens/ScreenUtils/BottomSheetUtils.dart';
import 'package:myluck/Screens/InfoScreens/ContactUsScreen.dart';
import 'package:myluck/Screens/LeaderBoardScreen.dart';
import 'package:myluck/Screens/Login/LoginScreen.dart';
import 'package:myluck/Screens/Notifications/NotificationsScreen.dart';

import '../main.dart';

class NavigatorUtils {
  static void moveToLogin(BuildContext context) {
    Navigator.popUntil(context, ModalRoute.withName('/'));
    _moveToWidget(context, LoginScreen());
  }

  static void moveToDashBoard(BuildContext context) {
    _moveToWidget(context, mainScreen);
  }

  static void moveToLeaderBoard(BuildContext context) {
    _moveToWidget(context, LeaderBoardScreen());
  }

  static void moveToNotifications(BuildContext context) {
    _moveToWidget(context, NotificationsScreen());
  }

  static void moveToContactUsBoard(BuildContext context) {
    _moveToWidget(context, ContactUsScreen());
  }

  static void moveToAboutAppBoard(BuildContext context) {
    _moveToWidget(context, AboutAppScreen());
  }

  static void _moveToWidget(BuildContext context, Widget _childWidget) {
    Navigator.push(
        context,
        CupertinoPageRoute(
            builder: (BuildContext context) => _childWidget));
  }

  static void moveToLogoutScreen(BuildContext context) async {
    BottomSheetUtils.showBottomFilterSheetLogout(context);
  }
}
