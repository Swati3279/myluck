
import 'package:flutter/foundation.dart';

class PrintUtils {

  static bool _isDebug = kDebugMode;

  static void printLog(String message) {
    _print(message, _isDebug);
  }

  static void _print(String message, bool isDebug) {
    if(isDebug) {
      print(message);
    }
  }
}