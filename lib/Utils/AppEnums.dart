
class AppEnums {

}

enum EnumAppNavigationMenus { LEADER_BOARD, NOTIFICATIONS, CONTACT_US, ABOUT_APP }

enum EnumAppTheme {
  LIGHT, DARK
}

enum EnumMenuState {
  closed,
  opening,
  open,
  closing,
}

enum ConnectivityStatus {
  WiFi,
  Cellular,
  Offline
}