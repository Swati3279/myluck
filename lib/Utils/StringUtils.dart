
class StringUtils {

  static String appName = "MyLuck";

  static String LeaderBoard = "Leader Board";
  static String Notifications = "Notifications";
  static String ContactUs = "Contact Us";
  static String AboutApp = "About App";


  static String editProfile = "Edit Profile";
  static String weRespectYourPrivacy ="We respect your privacy. Your number is safe and will not be shared with anyone.";

  static String logout = "Log Out";
  static String logoutConfirmationSubtext = "Are you sure you want to Logout?";

  static String getInitials(String _fullText, {int numWords = 2}) {
    List<String> names = _fullText.split(" ");
    String initials = "";

    if (numWords < names.length) {
      numWords = names.length;
    }
    for (var i = 0; i < numWords; i++) {
      initials += '${names[i][0]}';
    }
    return initials;
  }

  static String getInitialName(String _fullText) {
    List<String> names = _fullText.split(" ");
    String initialName = "";

    if ((names?.length ?? 0) > 0) {
      initialName = names[0];
    }

    return initialName;
  }
}