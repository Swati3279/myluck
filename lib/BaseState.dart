import 'dart:async';

import 'package:flutter/material.dart';
import 'Constants/Constant.dart';
import 'Provider/AppMetaProvider.dart';
import 'Provider/ConnectivityProvider.dart';
import 'Utils/AppUtils.dart';
import 'Utils/ColorUtils.dart';

class BaseState extends StatefulWidget {
  static BuildContext baseContext;
  final Widget child;

  BaseState({this.child});

  @override
  _BaseStateState createState() => new _BaseStateState();
}

class _BaseStateState extends State<BaseState> with WidgetsBindingObserver {

  bool _isClosedByUser = false;
  bool _isConnected = true;

  double _deviceWidth = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    triggerObservers();
    //checkInternetConnectivity();
  }

  void triggerObservers() {
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    //alertDialog(context);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //_isConnected = ConnectivityProvider.isConnected(context);
    _deviceWidth = AppMetaProvider.getDeviceWidth(context);

    return Material(
      child: Directionality(
        textDirection: TextDirection.ltr,
        child: _mainWidget(),
      ),
    );
  }

  Widget _mainWidget() {
    return Container(
        child: Stack(
      children: <Widget>[
        widget.child,
        _internetConnection(),
      ],
    ));
  }

  Widget _internetConnection() {

    return !_isConnected && !_isClosedByUser ? Positioned(
        width: _deviceWidth,
        bottom: 0.0,
        child:  Container(
          width: _deviceWidth,
          color: ColorUtils.transparentThemeBackground,
          padding:  EdgeInsets.all(4.0),
          child: Container(
            padding: EdgeInsets.all(Constant.appDefaultSpacingHalf),
            child: Row(
              children: <Widget>[
                Expanded(child: Text("Your device is not connected to internet, please make sure your connection is working.", style: TextStyle(color: ColorUtils.whiteColor, fontSize: 12.0), textDirection: TextDirection.ltr,)),
                IconButton(icon: Icon(Icons.close), color: ColorUtils.whiteColor, onPressed: () {
                  _isClosedByUser = true;
                  AppUtils.refreshCurrentState(this);
                  Timer.periodic(Duration(seconds: 2), (timer) {
                    _isClosedByUser = false;
                  });
                }),
              ],
            ),
          ),
        ),
      ) : Container();
  }

  AppLifecycleState _appLifecycleState;

  // TODO: DID_CHANGE_APP_LIFE_CYCLE
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {
      _appLifecycleState = state;
      if (_appLifecycleState == AppLifecycleState.paused ||
          _appLifecycleState == AppLifecycleState.inactive) {
        print("IF timer---fired: $_appLifecycleState");
      } else {
        print("ELSE timer---fired: $_appLifecycleState");
      }
    });
  }
}
