import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:myluck/Provider/AppMetaProvider.dart';
import 'package:provider/provider.dart';
import 'BaseState.dart';
import 'Constants/ConstantAppRoute.dart';
import 'Menu/menu_page.dart';
import 'Menu/zoom_scaffold.dart';
import 'Network/CustomHttpOverrides.dart';
import 'Provider/AccountProvider.dart';
import 'Provider/ConnectivityProvider.dart';
import 'Provider/EditProfileProvider.dart';
import 'Provider/LeaderBoardProvider.dart';
import 'Provider/Menu3DProvider.dart';
import 'Screens/Dashboard.dart';
import 'Screens/Login/LoginScreen.dart';

Widget mainScreen;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  try {
    await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  } catch (e) {}

  runApp(
    /// Providers are above [MyApp] instead of inside it, so that tests
    /// can use [MyApp] while mocking the providers
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => AppMetaProvider(),
          lazy: false,
        ),
        /*ChangeNotifierProvider(
          create: (_) => ConnectivityProvider(),
          lazy: false,
        ),*/
        ChangeNotifierProvider(
          create: (_) => AccountProvider(),
          lazy: false,
        ),
        ChangeNotifierProvider(
          create: (_) => EditProfileProvider(),
          lazy: false,
        ),
        ChangeNotifierProvider(
          create: (_) => LeaderBoardProvider(),
          lazy: false,
        ),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatefulWidget {
  static final navKey = new GlobalKey<NavigatorState>();

  @override
  State createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  bool _isFirstTimeScreenCalling = true;

  Menu3DProvider menuController;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
    if (mainScreen == null) {
      menuController = new Menu3DProvider(
        vsync: this,
      );

      mainScreen = ChangeNotifierProvider.value(
          value: menuController, child: getZoomScaffold());
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_isFirstTimeScreenCalling) {
      _isFirstTimeScreenCalling = false;
    }
  }

  Widget getZoomScaffold() {
    return ZoomScaffold(
      menuScreen: MenuScreen(),
      contentScreen: Layout(contentBuilder: (cc) => Dashboard()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseState(
      child: MaterialApp(
        navigatorKey: MyApp.navKey,
        debugShowCheckedModeBanner: false,
        initialRoute: ConstantAppRoute.initialAppScreen,
        routes: <String, WidgetBuilder>{
          ConstantAppRoute.initialAppScreen: (context) => LoginScreen(),
        },
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }
}
