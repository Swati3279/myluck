import 'dart:async';
import 'dart:convert';

import 'package:myluck/EventBus/EventBusUtils.dart';
import 'package:myluck/Network/Model/UserDetail.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefUtil {
  static const String _KEY_LOGIN_DATA = "loginData";

  static List<String> alSpKeysToRemove = {
    _KEY_LOGIN_DATA,
  }.toList();

  ///
  /// Login Methods - START
  ///

  static Future<bool> _saveLoginDataJson(String loginData) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(_KEY_LOGIN_DATA, loginData);
    return true;
  }

  static Future<bool> saveLoginData(UserDetail loginData) async {
    Map<String, dynamic> res = loginData.toJson();
    String jsonLogin = json.encode(res);
    if (loginData != null) {
      await _saveLoginDataJson(jsonLogin);
      EventBusUtils.eventLogin();
      return true;
    } else {
      return false;
    }
  }

  static Future<UserDetail> getLoginData() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      String loginData = prefs.getString(_KEY_LOGIN_DATA);

      Map<String, dynamic> user = json.decode(loginData);

      UserDetail loginRes = new UserDetail.fromJson(user);
      return loginRes;
    } catch (e) {}
    return null;
  }

  static Future<String> getLoggedInUserId() async {
    try {
      final loginRes = await getLoginData();
      return loginRes.phone;
    } catch (e) {}

    return null;
  }

  static Future<bool> isLogin() async {
    try {
      final loginID = await getLoggedInUserId();
      return loginID != null;
    } catch (e) {}

    return false;
  }

  ///
  /// Login Methods - END
  ///

  ///
  /// Logout Methods - START
  ///

  static Future<void> logout() async {
    final prefs = await SharedPreferences.getInstance();

    for (int index = 0; index < (alSpKeysToRemove?.length ?? 0); index++) {
      String spKeysToRemove = alSpKeysToRemove[index];
      await prefs.remove(spKeysToRemove);
    }
  }

  ///
  /// Logout Methods - END
  ///
}
