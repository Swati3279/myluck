import 'dart:async';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:myluck/AssetContstants.dart';
import 'package:myluck/Network/Model/UserDetail.dart';
import 'package:myluck/Utils/AppUtils.dart';
import 'package:myluck/Utils/PrintUtils.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() {
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<MyHomePage> {

  num _diceNumber = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('My Luck')),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {

    _getUserDetail();
    
    var userStreamWidget = StreamBuilder<QuerySnapshot>(
      stream: FirebaseFirestore.instance.collection('users').snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return LinearProgressIndicator();

        return _buildList(context, snapshot.data.docs);
      },
    );

    return Column(
      children: [
        _diceUI(),
        _rollDice(),
        //userStreamWidget,
      ],
    );
  }

  Future _getUserDetail() async {
    var userSnapshot = FirebaseFirestore.instance.collection("users").doc('9711915169').snapshots();

    FirebaseFirestore.instance.collection("users").doc('9711915160').collection('9711915160').add({

    });

    userSnapshot.listen((event) {
      final record = UserDetail.fromSnapshot(event);
      PrintUtils.printLog("userSnapshot: ${record.toString()}");
    }, onError: (_e) {
      PrintUtils.printLog("userSnapshot: ERROR");
    });
    
  }

  Widget _diceUI() {
    return Center(
      child: Container(
        width: 100,
        height: 100,
          child: Image.asset('assets/dice_$_diceNumber.png')),
    );
  }

  Widget _rollDice() {
    return RaisedButton(
      child: Text('Roll the Dice!!'),
        onPressed: () {
      _animateDice();
    });
  }

  int animationSecondsAlpha = 1000 * 5;
  int animationDuration = 150;
  int animationMillisecondCount = 0;
  void _animateDice() {
    animationMillisecondCount = 0;
    Timer.periodic(Duration(milliseconds: animationDuration), (timer) {

      _diceNumber = _getRandomNumber();
      PrintUtils.printLog("_diceNumber: $_diceNumber");
      AppUtils.refreshCurrentState(this);

      animationMillisecondCount += animationDuration;
      if(animationMillisecondCount > animationSecondsAlpha) {
        timer.cancel();
      }
    });
  }

  Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 20.0),
      children: snapshot.map((data) => _buildListItem(context, data)).toList(),
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot data) {
    final record = UserDetail.fromSnapshot(data);

    return Padding(
      key: ValueKey(record.name),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: ListTile(
          title: Text(record.toString()),
          onTap: () {
            record.reference.update({'score': record.score + _getRandomNumber(), 'total_chance_count': record.totalChanceCount + 1});
          }
        ),
      ),
    );
  }
}

num _getRandomNumber() {
  return Random().nextInt(6) + 1;
}