import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:myluck/AssetContstants.dart';
import 'package:myluck/Constants/Constant.dart';
import 'package:myluck/CustomWidgets/CustomText.dart';
import 'package:myluck/CustomWidgets/CustomWidget.dart';
import 'package:myluck/Network/Model/UserDetail.dart';
import 'package:myluck/Provider/AccountProvider.dart';
import 'package:myluck/Provider/Menu3DProvider.dart';
import 'package:myluck/SharedPref/SharedPrefUtil.dart';
import 'package:myluck/Utils/AppEnums.dart';
import 'package:myluck/Utils/AppUtils.dart';
import 'package:myluck/Utils/ColorUtils.dart';
import 'package:myluck/Utils/NavigatorUtils.dart';
import 'package:myluck/Utils/StringUtils.dart';

class MenuScreen extends StatefulWidget {
  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {

  bool _isFirstTimeScreenCalling = true;

  UserDetail _loginResponse;
  EnumAppNavigationMenus _selectedMenu;
  final List<MenuItem> options = List();

  bool _isLogin;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

    if (_isFirstTimeScreenCalling) {
      _isFirstTimeScreenCalling = false;
      _initialiseListeners();
    }
  }

  void _initialiseListeners() {
    /*eventBus.on<EventUserProfileChanged>().listen((_event) {
      if (_event?.userProfileData?.id == _loginResponse?.data?.id) {
        _loginResponse?.data = _event.userProfileData;
        AppUtils.refreshCurrentState(this);
      }
    });*/
  }

  void reset() {
    _selectedMenu = null;
  }

  bool _isMenuSelected(EnumAppNavigationMenus _selectedMenu) => this._selectedMenu == _selectedMenu;

  void _clickMenu(EnumAppNavigationMenus _selectedMenu) {
    this._selectedMenu = _selectedMenu;
    AppUtils.refreshCurrentState(this);

    Menu3DProvider.eventToggleRead(context);

    switch(_selectedMenu) {
      case EnumAppNavigationMenus.LEADER_BOARD:
        _eventLeaderBoard();
        break;
      case EnumAppNavigationMenus.NOTIFICATIONS:
        _eventClickNotifications();
        break;
      case EnumAppNavigationMenus.CONTACT_US:
        _eventClickContactUs();
        break;
      case EnumAppNavigationMenus.ABOUT_APP:
        _eventClickAboutApp();
        break;
    }
  }

  void _eventLeaderBoard() {
    NavigatorUtils.moveToLeaderBoard(context);
  }

  void _eventClickNotifications() {
    NavigatorUtils.moveToNotifications(context);
  }

  void _eventClickContactUs() {
    NavigatorUtils.moveToContactUsBoard(context);
  }

  void _eventClickAboutApp() {
    NavigatorUtils.moveToAboutAppBoard(context);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    options.add(MenuItem(EnumAppNavigationMenus.LEADER_BOARD, AssetContstants.ICON_HEART_NORMAL, StringUtils.LeaderBoard, isLoginRequired: true));
    options.add(MenuItem(EnumAppNavigationMenus.NOTIFICATIONS, AssetContstants.ICON_NOTIFICATION, StringUtils.Notifications, isLoginRequired: true, iconSize: 14.0));
    options.add(MenuItem(EnumAppNavigationMenus.CONTACT_US, AssetContstants.ICON_BOOMARK_MENU, StringUtils.ContactUs, isLoginRequired: false));
    options.add(MenuItem(EnumAppNavigationMenus.ABOUT_APP, AssetContstants.ICON_SETTINGS, StringUtils.AboutApp, isLoginRequired: false));
  }

  @override
  Widget build(BuildContext context) {
    _isLogin = AccountProvider.isLoggedInWatch(context);
    _loginResponse = AccountProvider.getLoginUserInfoWatch(context);

    return GestureDetector(
      onPanUpdate: (details) {
        //on swiping left
        if (details.delta.dx < -6) {
          Menu3DProvider.eventToggleSelect(context);
        }
      },
      child: Container(
        padding: EdgeInsets.only(
            right: MediaQuery.of(context).size.width / 2.2),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            getUserWidgetUI(),
            Spacer(),
            getMenuWidgets(),
            Spacer(),
            Spacer(),
            Spacer(),
            _logoutWidget(),
          ],
        ),
      ),
    );
  }

  Widget getMenuWidgets() {
    return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: options.map((item) {
              return buildListTileMenu(item);
            }).toList(),
          );
  }

  Widget getUserWidgetUI() {
    Widget childWidget = _isLogin ? _loggedInUserWidget() : _doLoginUIWidget();

    if(_isLogin) {
      childWidget = Container(
        padding: EdgeInsets.only(top: (Constant.appDefaultSpacingHalf * 10), left: Constant.appDefaultSpacing),
        child: _loggedInUserWidget(),
      );
    } else {
      childWidget = Container(
        padding: EdgeInsets.only(top: (Constant.appDefaultSpacingHalf * 10)),
        child: _doLoginUIWidget(),
      );
    }

    return childWidget;
  }

  Widget buildListTileMenu(MenuItem item) {
    if (item == null) {
      return Container();
    }

    bool _isVisible = true;

    if(item.isLoginRequired ?? false) {
      _isVisible = _isLogin;
    }

    var _color = _isMenuSelected(item._menuId) ? ColorUtils.primaryColor : Colors.transparent;
    var _selectionTextColor = _isMenuSelected(item._menuId) ? ColorUtils.whiteColor : ColorUtils.dashboardMenuTextColor;
    var _selectionIconColor = _isMenuSelected(item._menuId) ? ColorUtils.whiteColor : ColorUtils.dashboardIconColor;

    var _iconSize = item.iconSize ?? 16.0;

    var _radius = Radius.circular(Constant.appDefaultSpacingHalf);

    return _isVisible ? GestureDetector(
      onTap: () {
        _clickMenu(item._menuId);
      },
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(topRight: _radius, bottomRight: _radius),
              boxShadow: [
                new BoxShadow(
                  color: _color,
                ),
              ],
            ),
            padding: EdgeInsets.only(left: Constant.appDefaultSpacing, right: Constant.appDefaultSpacing, top: Constant.appDefaultSpacing, bottom: Constant.appDefaultSpacing),
            child: Row(
              children: <Widget>[
                CustomWidget.getDefaultWidthSizedBox(width: Constant.appDefaultSpacingHalf),
                SvgPicture.asset(
                  item.icon,
                  width: _iconSize,
                  color: _selectionIconColor,
                ),
                CustomWidget.getDefaultWidthSizedBox(width: Constant.appDefaultSpacingHalf * 3),
                CustomText.text16Regular(context, item.title, textColor: _selectionTextColor),
              ],
            ),
          ),
          CustomWidget.getDefaultHeightSizedBox(height: Constant.appDefaultSpacingHalf / 2),
        ],
      ),
    ) : Container();
  }

  Widget _loggedInUserWidget() {
    return GestureDetector(
      onTap: () {
        _moveToUserProfile();
      },
      child: Container(
        child: Row(
          children: <Widget>[
            CustomWidget.getCircularUserProfilePic(_loginResponse?.name, width: 55.0, height: 55.0),
            /*
            Container(
              height: 55.0,
              width: 55.0,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/Oval.png'), fit: BoxFit.fill),
                  borderRadius: BorderRadius.circular(30.0),
                  color: Colors.grey),
            ),*/
            CustomWidget.getDefaultWidthSizedBox(width: Constant.appDefaultSpacingSemiHalf),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CustomText.text18Regular(context, _loginResponse?.name ?? "", textOverflow: TextOverflow.ellipsis),
                  GestureDetector(
                    child: Container(
                      padding: EdgeInsets.only(top: Constant.appDefaultSpacingHalf / 2),
                      child: CustomText.text16Regular(context, StringUtils.editProfile),
                    ),
                    onTap: () {
                      //NavigatorUtils.navigateToProfileSettings(context);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _logoutWidget() {
    return Visibility(
      visible: _isLogin ?? false,
      child: InkWell(
        onTap: () {
          //_logoutClickEvent();
          NavigatorUtils.moveToLogoutScreen(context);
        },
        child: Container(
          padding: EdgeInsets.only(bottom: (Constant.appDefaultSpacingHalf * 3), left: Constant.appDefaultSpacing),
          child: Row(
            children: <Widget>[
              SvgPicture.asset(
                AssetContstants.ICON_LOGOUT,
                width: 16,
              ),
              CustomWidget.getDefaultWidthSizedBox(width: Constant.appDefaultSpacingHalf),
              CustomText.text18Regular(context, StringUtils.logout, textColor: ColorUtils.logoutColor),
             /* CustomWidget.getText(
                StringUtils.logout,
                style: Theme.of(context).textTheme.bodyText1.copyWith(color: ColorUtils.redColor),
              ),*/

            ],
          ),
        ),
      ),
    );
  }

  Widget _doLoginUIWidget() {
    return Container(
      padding: EdgeInsets.all(Constant.appDefaultSpacingHalf),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            CustomText.text18Bold(context, 'Welcome to MyLuck'),
            CustomWidget.getDefaultHeightSizedBox(height: Constant.appDefaultSpacingHalf / 2),
            CustomWidget.getDefaultHeightSizedBox(height: Constant.appDefaultSpacingHalf),
            Container(
              width: (MediaQuery.of(context).size.width / 1),
              child: CustomWidget.getRoundedRaisedButton(context, "Login", padding: Constant.appDefaultSpacingSemiHalf, onPressed: () {
                //AccountProvider.checkLoginAndMoveRed(context, null);
              }),
            ),
          ],
        ),
      ),
    );
  }

  void _moveToUserProfile() async {
    String _userId = await SharedPrefUtil.getLoggedInUserId();
    //NavigatorUtils.moveToCommonUserProfileScreen(context, _userId);
  }
}

class MenuItem {
  EnumAppNavigationMenus _menuId;
  String title;
  String icon;
  double iconSize;
  bool isLoginRequired;
  bool isSelected;

  MenuItem(this._menuId, this.icon, this.title,
      {this.isLoginRequired = false,
      this.isSelected = false, this.iconSize = 16.0});
}
