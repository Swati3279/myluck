import 'package:flutter/material.dart';
import 'package:myluck/Provider/Menu3DProvider.dart';
import 'package:myluck/Utils/AppEnums.dart';
import 'package:myluck/Utils/ColorUtils.dart';

class ZoomScaffold extends StatefulWidget {
  final Widget menuScreen;
  final Layout contentScreen;

  ZoomScaffold({
    this.menuScreen,
    this.contentScreen,
  });

  @override
  _ZoomScaffoldState createState() => new _ZoomScaffoldState();
}

class _ZoomScaffoldState extends State<ZoomScaffold>
    with TickerProviderStateMixin {

  EnumMenuState state;

  Curve scaleDownCurve = new Interval(0.0, 0.3, curve: Curves.easeOut);
  Curve scaleUpCurve = new Interval(0.0, 1.0, curve: Curves.easeOut);
  Curve slideOutCurve = new Interval(0.0, 1.0, curve: Curves.easeOut);
  Curve slideInCurve = new Interval(0.0, 1.0, curve: Curves.easeOut);

  Widget createContentDisplay(BuildContext context) {

    return zoomAndSlideContent(context, Container(
      child: widget.contentScreen.contentBuilder(context),
    ));
  }

  Widget zoomAndSlideContent(BuildContext context, Widget content) {
    var slidePercent, scalePercent;

    switch (state) {
      case EnumMenuState.closed:
        slidePercent = 0.0;
        scalePercent = 0.0;
        break;
      case EnumMenuState.open:
        slidePercent = 1.0;
        scalePercent = 1.0;
        break;
      case EnumMenuState.opening:
        slidePercent = slideOutCurve.transform(Menu3DProvider.getPercentageOpenSelect(context));
        scalePercent = scaleDownCurve.transform(Menu3DProvider.getPercentageOpenSelect(context));
        break;
      case EnumMenuState.closing:
        slidePercent = slideInCurve.transform(Menu3DProvider.getPercentageOpenSelect(context));
        scalePercent = scaleUpCurve.transform(Menu3DProvider.getPercentageOpenSelect(context));
        break;
    }

    final slideAmount = 240.0 * slidePercent;
    final contentScale = 1.0 - (0.2 * scalePercent);
    final cornerRadius =
        16.0 * Menu3DProvider.getPercentageOpenSelect(context);

    return new Transform(
      transform: new Matrix4.translationValues(slideAmount, 0.0, 0.0)
        ..scale(contentScale, contentScale),
      alignment: Alignment.centerLeft,
      child: new Container(
        decoration: new BoxDecoration(
          boxShadow: [
            new BoxShadow(
              color: Colors.black12,
              offset: const Offset(0.0, 5.0),
              blurRadius: 15.0,
              spreadRadius: 10.0,
            ),
          ],
        ),
        child: new ClipRRect(
            borderRadius: new BorderRadius.circular(cornerRadius),
            child: content),
      ),
    );
  }

  Widget getMainWidget(Widget content) {
    if(state == EnumMenuState.open) {
      return Container(
        color: ColorUtils.blackColor,
        child: content,
      );
    } else {
      return content;
    }
  }

  @override
  Widget build(BuildContext context) {
    state = Menu3DProvider.getMenuStateWatch(context);

    return Stack(
      children: [
        Container(
          child: Scaffold(
            body: widget.menuScreen,
          ),
        ),
        createContentDisplay(context)
      ],
    );
  }
}

typedef Widget ZoomScaffoldBuilder(
    BuildContext context, Menu3DProvider Menu3DProvider);

class Layout {
  final WidgetBuilder contentBuilder;

  Layout({
    this.contentBuilder,
  });
}