import 'dart:async';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myluck/Constants/Constant.dart';
import 'package:myluck/CustomWidgets/CustomText.dart';
import 'package:myluck/CustomWidgets/CustomWidget.dart';
import 'package:myluck/EventBus/EventBusUtils.dart';
import 'package:myluck/EventBus/EventModels/Event3DNavigationDrawerState.dart';
import 'package:myluck/Network/Model/UserDetail.dart';
import 'package:myluck/Provider/AccountProvider.dart';
import 'package:myluck/Provider/Menu3DProvider.dart';
import 'package:myluck/Utils/AppEnums.dart';
import 'package:myluck/Utils/ColorUtils.dart';
import 'package:myluck/Utils/NavigatorUtils.dart';
import 'package:myluck/Utils/PrintUtils.dart';
import 'package:myluck/Utils/ToastUtils.dart';
import '../BaseState.dart';
import '../Utils/AppUtils.dart';
import 'UserDetailUI.dart';


class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard>
    with AutomaticKeepAliveClientMixin, WidgetsBindingObserver {

  bool _screenInitializationCompleted = false;
  bool _isDrawerOpen = false;

  UserDetail _loginResponse;

  num _diceNumber = 1;

  @override
  initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_screenInitializationCompleted) {
      _screenInitializationCompleted = true;

      Constant.initialize(context);
      _setObservables();

      BaseState.baseContext = context;
    }

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  void _setObservables() {
    eventBus.on<Event3DNavigationDrawerState>().listen((event) {
      _isDrawerOpen = (event.menuState == EnumMenuState.open);
      AppUtils.refreshCurrentState(this);
    });
  }


  @override
  Widget build(BuildContext context) {

    _loginResponse = AccountProvider.getLoginUserInfoWatch(context);

    return Container(
      child: Stack(
        children: <Widget>[
          Scaffold(
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            resizeToAvoidBottomPadding: true,
            appBar: null,
            body: SafeArea(child: _buildMainHomeView()),
          ),
          Visibility(
            visible: _isDrawerOpen,
            child: GestureDetector(
              onTap: () {
                Menu3DProvider.eventToggleRead(context);
              },
              child: Container(
                width: 50000,
                height: 50000,
                color: ColorUtils.dialogBackgroundTransparentColorOpacity60,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildMainHomeView() {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      resizeToAvoidBottomPadding: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(45.0),
        child: AppBar(
          leading: Container(),
          backgroundColor: ColorUtils.primaryColor,
          flexibleSpace: _homeHeaderWidget(),
        ),
      ),
      body: _buildBody(),
    );
  }

  Widget _homeHeaderWidget() {
    return Container(
      decoration: BoxDecoration(
        color: ColorUtils.primaryColor,
        boxShadow: [
          BoxShadow(
            color: ColorUtils.primaryColor,
            offset: Offset(0.0, 5.0), //(x,y)
            blurRadius: 40.0,
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: Constant.appDefaultSpacingHalf, right: Constant.appDefaultSpacingHalf, top:  Constant.appDefaultSpacingHalf / 2, bottom:  Constant.appDefaultSpacingHalf / 2),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: InkWell(
                onTap: () {
                  Menu3DProvider.eventToggleRead(context);
                },
                child: Stack(
                  children: <Widget>[
                    CustomWidget.getCircularUserProfilePic(_loginResponse?.name ?? '--', width: 30.0, height: 30.0),
                    Positioned(
                      left: 20.0,
                      top: 15.0,
                      child: Container(
                        alignment: Alignment.center,
                        height: 15.0,
                        width: 15.0,
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[400],
                                blurRadius: 4.0,
                              ),
                            ],
                            borderRadius: BorderRadius.circular(20.0),
                            color: Colors.white),
                        child: Image.asset(
                          'assets/CombinedShape.png',
                          scale: 2,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBody() {
    return Container(
      padding: EdgeInsets.all(Constant.appDefaultSpacing),
        child: Column(
          children: [
            CustomWidget.getDefaultHeightSizedBox(),
            UserDetailUI(),
            (_loginResponse?.totalChanceCount ?? 0) < 10 ? _mainDiceUI() : _noChanceLeftUI(),
          ],
        )
    );
  }

  Widget _mainDiceUI() {
    return Container(
        child: Column(
          children: [
            CustomWidget.getDefaultHeightSizedBox(),
            _diceUI(),
            CustomWidget.getDefaultHeightSizedBox(),
            _rollDice(),
          ],
        )
    );
  }

  Widget _diceUI() {
    return Center(
      child: Container(
          width: 100,
          height: 100,
          child: Image.asset('assets/dice_$_diceNumber.png')),
    );
  }

  Widget _rollDice() {
    return CustomWidget.getRoundedRaisedButton(context, 'Roll the Dice!!', onPressed: () {
      _animateDice();
    });
  }

  Widget _noChanceLeftUI() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CustomWidget.getDefaultHeightSizedBox(),
        CustomWidget.getDefaultHeightSizedBox(),
        CustomWidget.getDefaultHeightSizedBox(),
        CustomText.text18Bold(context, 'No Chances left. You can check the LeaderBoard for your position', textAlign: TextAlign.center),
        CustomWidget.getDefaultHeightSizedBox(),
        CustomWidget.getRoundedRaisedButton(context, 'Move to LeaderBoard !!', onPressed: () {
          NavigatorUtils.moveToLeaderBoard(context);
        })
      ],
    ) ;
  }

  int animationSecondsAlpha = 1000 * 3;
  int animationDuration = 150;
  int animationMillisecondCount = 0;
  bool _isDiceRolling = false;
  void _animateDice() {
    if(!_isDiceRolling) {
      _isDiceRolling = true;
      animationMillisecondCount = 0;

      Timer.periodic(Duration(milliseconds: animationDuration), (timer) {
        _diceNumber = _getRandomNumber();
        PrintUtils.printLog("_diceNumber: $_diceNumber");
        AppUtils.refreshCurrentState(this);

        animationMillisecondCount += animationDuration;
        if (animationMillisecondCount > animationSecondsAlpha) {
          _isDiceRolling = false;
          timer.cancel();

          ToastUtils.showToast(context, 'You got: $_diceNumber');
          AccountProvider.updateScoreOfUser(context, _diceNumber);
        }
      });
    } else {
      ToastUtils.showToast(context, 'Dice Rolling, Please wait...');
    }
  }

  num _getRandomNumber() {
    return Random().nextInt(6) + 1;
  }

  @override
  bool get wantKeepAlive {
    return true;
  }
}
