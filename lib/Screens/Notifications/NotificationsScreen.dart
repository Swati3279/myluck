import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:myluck/AssetContstants.dart';
import 'package:myluck/Constants/Constant.dart';
import 'package:myluck/CustomWidgets/CustomText.dart';
import 'package:myluck/CustomWidgets/CustomWidget.dart';
import 'package:myluck/EventBus/EventBusUtils.dart';
import 'package:myluck/EventBus/EventModels/EventLogin.dart';
import 'package:myluck/Network/Model/UserDetail.dart';
import 'package:myluck/Provider/AccountProvider.dart';
import 'package:myluck/Screens/Notifications/EmptyNotificationScreen.dart';
import 'package:myluck/SharedPref/SharedPrefUtil.dart';
import 'package:myluck/Utils/ColorUtils.dart';
import 'package:myluck/Utils/NavigatorUtils.dart';
import 'package:myluck/Utils/StringUtils.dart';
import 'package:myluck/Utils/ToastUtils.dart';

class NotificationsScreen extends StatefulWidget {
  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {

  bool _screenInitializationCompleted = false;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    if (!_screenInitializationCompleted) {
      _screenInitializationCompleted = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomWidget.getCustomScaffold(context, StringUtils.Notifications, _mainBodyUI());
  }

  Widget _mainBodyUI() {
    return Center(
      child: Container(
        child: EmptyNotificationScreen(),
      ),
    );
  }
}
