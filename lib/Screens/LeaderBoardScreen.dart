import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:myluck/AssetContstants.dart';
import 'package:myluck/Constants/Constant.dart';
import 'package:myluck/CustomWidgets/CustomText.dart';
import 'package:myluck/CustomWidgets/CustomWidget.dart';
import 'package:myluck/EventBus/EventBusUtils.dart';
import 'package:myluck/EventBus/EventModels/EventLogin.dart';
import 'package:myluck/Network/Model/UserDetail.dart';
import 'package:myluck/Provider/AccountProvider.dart';
import 'package:myluck/Provider/LeaderBoardProvider.dart';
import 'package:myluck/Screens/UserDetailUI.dart';
import 'package:myluck/SharedPref/SharedPrefUtil.dart';
import 'package:myluck/Utils/ColorUtils.dart';
import 'package:myluck/Utils/NavigatorUtils.dart';
import 'package:myluck/Utils/StringUtils.dart';
import 'package:myluck/Utils/ToastUtils.dart';

class LeaderBoardScreen extends StatefulWidget {
  @override
  _LeaderBoardScreenState createState() => _LeaderBoardScreenState();
}

class _LeaderBoardScreenState extends State<LeaderBoardScreen> {

  bool _screenInitializationCompleted = false;
  ScrollController scrollControllerItemListing;

  List<DocumentSnapshot> _listOfUsers;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    if (!_screenInitializationCompleted) {
      _screenInitializationCompleted = true;
      LeaderBoardProvider.loadInitialData(context);
      _setScrollController();
    }
  }

  void _setScrollController() {
    var initialScrollOffset = 0.0;
    scrollControllerItemListing = new ScrollController(initialScrollOffset: initialScrollOffset);
  }

  @override
  Widget build(BuildContext context) {
    _listOfUsers = LeaderBoardProvider.getLeaderBoardUserListingWatch(context);
    return CustomWidget.getCustomScaffold(context, StringUtils.LeaderBoard, _mainBodyUI());
  }

  Widget _mainBodyUI() {
    return Container(
      padding: EdgeInsets.all(Constant.appDefaultSpacingHalf),
      child: Column(
        children: [
          CustomText.text22SemiBold(context, 'Your Score'),
          CustomWidget.getDefaultHeightSizedBox(),
          UserDetailUI(),
          CustomWidget.getDefaultHeightSizedBox(),
          CustomText.text22SemiBold(context, 'LeaderBoard'),
          Expanded(child: _getContentUIList()),
        ],
      ),
    );
  }

  Widget _getContentUIList() {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      child: NotificationListener<ScrollNotification>(
        //key: riContentListingKey1,
        onNotification: (ScrollNotification scrollInfo) {
          double currentPixel = scrollInfo.metrics.pixels;

          if (currentPixel == scrollInfo.metrics.maxScrollExtent) {
            LeaderBoardProvider.loadMoreData(context);
          }

          return true;
        },
        child: listViewContent(_listOfUsers),
      ),
    );
  }


  Widget listViewContent(List<DocumentSnapshot> _items) {
    return ListView.separated(
        controller: scrollControllerItemListing,
        physics: AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        separatorBuilder: (BuildContext contentListContext, int index) =>
            SizedBox(
              height: Constant.appListSeparatorHeight,
            ),
        itemCount: _items?.length ?? 0,
        //itemCount: 20,
        itemBuilder: (BuildContext context, int index) {
          var _item = _items[index];
          return _buildListItem(index, _item);
        });
  }

  Widget _buildListItem(int index, DocumentSnapshot data) {
    final record = UserDetail.fromSnapshot(data);

    return Padding(
      key: ValueKey(record.name),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Container(
          padding: EdgeInsets.all(Constant.appDefaultSpacingHalf),
          child: CustomText.text16Regular(context, '${index + 1}. ${record.toString()}'),
        ),
      ),
    );
  }
}
