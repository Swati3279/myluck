import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:myluck/AssetContstants.dart';
import 'package:myluck/Constants/Constant.dart';
import 'package:myluck/CustomWidgets/CustomText.dart';
import 'package:myluck/CustomWidgets/CustomWidget.dart';
import 'package:myluck/EventBus/EventBusUtils.dart';
import 'package:myluck/EventBus/EventModels/EventLogin.dart';
import 'package:myluck/Network/Model/UserDetail.dart';
import 'package:myluck/Provider/AccountProvider.dart';
import 'package:myluck/SharedPref/SharedPrefUtil.dart';
import 'package:myluck/Utils/ColorUtils.dart';
import 'package:myluck/Utils/NavigatorUtils.dart';
import 'package:myluck/Utils/StringUtils.dart';
import 'package:myluck/Utils/ToastUtils.dart';

class ContactUsScreen extends StatefulWidget {
  @override
  _ContactUsScreenState createState() => _ContactUsScreenState();
}

class _ContactUsScreenState extends State<ContactUsScreen> {

  bool _screenInitializationCompleted = false;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    if (!_screenInitializationCompleted) {
      _screenInitializationCompleted = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomWidget.getCustomScaffold(context, StringUtils.ContactUs, _mainBodyUI());
  }

  Widget _mainBodyUI() {
    return Center(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomText.text22SemiBold(context, 'Swati Chauhan', textColor: ColorUtils.redColor.withOpacity(0.8)),
            CustomText.text16Regular(context, '+91-9999999999'),
            CustomText.text16Regular(context, 'swatisschauhan@gmail.com'),
            CustomWidget.getDefaultHeightSizedBox(),
            CustomWidget.getDefaultHeightSizedBox(),
            CustomWidget.getDefaultHeightSizedBox(),
            CustomText.text18Regular(context, '\"  Sometime dark makes you more visible  \"', textColor: ColorUtils.dialogBlueColor),

            CustomWidget.getDefaultHeightSizedBox(),
            CustomWidget.getDefaultHeightSizedBox(),
            CustomWidget.getDefaultHeightSizedBox(),
            CustomWidget.getDefaultHeightSizedBox(),
          ],
        ),
      ),
    );
  }
}
