import 'dart:collection';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:myluck/Constants/Constant.dart';
import 'package:myluck/CustomWidgets/CustomText.dart';
import 'package:myluck/CustomWidgets/CustomWidget.dart';
import 'package:myluck/CustomWidgets/KeyboardAvoidingNew.dart';
import 'package:myluck/Utils/StringUtils.dart';
import '../Login/LogoutBottomSheet.dart';

class BottomSheetUtils {

  static Future<Object> showBottomFilterSheetLogout(BuildContext context,) async {
    Object bottomSheetResponse = await showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      enableDrag: true,
      isDismissible: true,
      //shape: getBottomSheetShape(),
      builder: (BuildContext context) {
        Widget child = Container(
          padding: EdgeInsets.all(Constant.appDefaultSpacing),
          child: LogoutBottomSheet(
            key: LogoutBottomSheet.filterKey,
          ),
        );

        return getMainBottomSheetWidget(context,
            title: StringUtils.logout, mainChild: child);
      },
    );

    return bottomSheetResponse;
  }

  static void showDialogQuickView(BuildContext context, Widget child) {
    Widget childWidget = Container(
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
        child: child,
      ),
    );

    showDialog(
        context: context,
        builder: (_) => Material(
              type: MaterialType.transparency,
              child: childWidget,
            ));
  }

  ///
  /// Common Methods
  ///

  static Widget getMainBottomSheetWidget(BuildContext context,
      {Widget mainChild, String title}) {
    return _getBottomSheetMainWidgetInsideContainer(context,
        child: KeyboardAvoidingNew(
          child: _getBottomSheetBackdropFilterWidget(context,
              child: Wrap(
                children: <Widget>[ 
                  Column(
                    children: <Widget>[
                      _getTitleWidget(context, title),
                      mainChild,
                    ],
                  ),
                ],
              )),
        ));
  }

  static Widget getTitleWidget(BuildContext context, String title) {
    return _getTitleWidget(context, title);
  }

  static Widget _getTitleWidget(BuildContext context, String title) {
    return ((title?.length ?? 0) > 0)
        ? Container(
            child: Column(
              children: <Widget>[
                CustomWidget.getDefaultHeightSizedBox(),
                CustomText.text18Regular(context, title,
                    textAlign: TextAlign.center),
                CustomWidget.getDefaultHeightSizedBox(),
                CustomWidget.getDivider(context),
                CustomWidget.getDefaultHeightSizedBox(),
              ],
            ),
          )
        : Container();
  }

  static ShapeBorder getBottomSheetShape() {
    return RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(0.0), topRight: Radius.circular(0.0)),
      //side: BorderSide(color: ColorUtils.primaryColor)
    );
  }

  static Widget _getBottomSheetMainWidgetInsideContainer(BuildContext context,
      {Widget child}) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              stops: [0.0, 0.1],
              colors: [Colors.transparent, Colors.white])),
      child: child,
    );
  }

  static Widget _getBottomSheetBackdropFilterWidget(BuildContext context,
      {Widget child}) {
    return Container(
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
        child: _getBottomSheetRoundedCornerWidget(context, child),
      ),
    );
  }

  static Widget getBlurredWidget(BuildContext context,
      {Widget child}) {
    return _getBottomSheetBackdropFilterWidget(context, child: child);
  }

  static Widget _getBottomSheetRoundedCornerWidget(
      BuildContext context, Widget child) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20), topRight: Radius.circular(20)),
      child: Container(
        color: Theme.of(context).bottomSheetTheme.backgroundColor,
        child: child,
      ),
    );
  }

  static Widget _singleRowSheetOptionWidget(
      BuildContext context, String text, Function onTap) {
    return InkWell(
      onTap: () {
        if (onTap != null) {
          onTap();
          Navigator.pop(context);
        }
      },
      child: Container(child: CustomText.text16Regular(context, text)),
    );
  }
}
