import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myluck/CustomWidgets/CustomText.dart';
import 'package:myluck/CustomWidgets/CustomWidget.dart';
import 'package:myluck/Provider/AccountProvider.dart';
import 'package:myluck/Utils/ColorUtils.dart';
import 'package:myluck/Utils/StringUtils.dart';

class LogoutBottomSheet extends StatefulWidget {
  static GlobalKey<_LogoutBottomSheetState> filterKey =
      GlobalKey<_LogoutBottomSheetState>();

  ScrollController scrollController;

  LogoutBottomSheet({Key key, this.scrollController}) : super(key: key);

  @override
  _LogoutBottomSheetState createState() => _LogoutBottomSheetState();
}

class _LogoutBottomSheetState extends State<LogoutBottomSheet>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _mainPart(),
    );
  }

  Widget _mainPart() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 16.0, right: 16.0),
            child: CustomText.text16RegularGrey(
                context, StringUtils.logoutConfirmationSubtext,
                textColor: ColorUtils.loginDialogGreyTextColor),
          ),
          CustomWidget.getDefaultHeightSizedBox(height: 24),
          CustomWidget.getRoundedRaisedButton(
            context,
            StringUtils.logout,
            buttonEnableColor: ColorUtils.logoutColor,
            onPressed: () {
              eventLogout();
            },
          ),
          CustomWidget.getDefaultHeightSizedBox(height: 24),
          Padding(
            padding: const EdgeInsets.only(left: 16.0, right: 16.0),
            child: CustomText.text14RegularGrey(
                context, StringUtils.weRespectYourPrivacy,
                textColor: ColorUtils.loginDialogGreyTextColor),
          ),
        ],
      ),
    );
  }

  void eventLogout() {
    AccountProvider.logoutRead(context);
  }
}
