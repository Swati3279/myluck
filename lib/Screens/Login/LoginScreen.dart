import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:myluck/AssetContstants.dart';
import 'package:myluck/Constants/Constant.dart';
import 'package:myluck/Constants/ConstantDB.dart';
import 'package:myluck/CustomWidgets/CustomText.dart';
import 'package:myluck/CustomWidgets/CustomWidget.dart';
import 'package:myluck/EventBus/EventBusUtils.dart';
import 'package:myluck/EventBus/EventModels/EventLogin.dart';
import 'package:myluck/Network/Model/UserDetail.dart';
import 'package:myluck/Provider/AccountProvider.dart';
import 'package:myluck/SharedPref/SharedPrefUtil.dart';
import 'package:myluck/Utils/ColorUtils.dart';
import 'package:myluck/Utils/NavigatorUtils.dart';
import 'package:myluck/Utils/ToastUtils.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _screenInitializationCompleted = false;

  bool _isLogin;
  UserDetail _loginResponse;

  var _controllerName = TextEditingController();
  var _controllerPhone = TextEditingController();

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    if (!_screenInitializationCompleted) {
      _screenInitializationCompleted = true;

      Constant.initialize(context);

      _setObservables();
      _checkLoginOrNot();
    }
  }

  void _checkLoginOrNot() async {
    _isLogin = await SharedPrefUtil.isLogin();
    if (_isLogin) {
      _moveToDashboard();
    }
  }

  void _moveToDashboard() {
    Navigator.pop(context);
    NavigatorUtils.moveToDashBoard(context);
  }

  void _setObservables() {
    eventBus.on<EventLogin>().listen((event) {
      if (event?.isLogin ?? false) {
        _moveToDashboard();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.green.withOpacity(0.7),
        body: buildBodyMain(),
      ),
    );
  }

  Widget buildBodyMain() {
    _isLogin = AccountProvider.isLoggedInWatch(context);
    _loginResponse = AccountProvider.getLoginUserInfoWatch(context);

    return Container(
      padding: EdgeInsets.all(Constant.appDefaultSpacing),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              width: 100,
              height: 100,
              child: Image.asset(AssetContstants.ICON_DICE_6)),
          CustomWidget.getDefaultHeightSizedBox(),
          CustomWidget.customTextFieldWithHeadingWithinContainer(
              context, 'User Name',
              textEditingController: _controllerName),
          CustomWidget.getDefaultHeightSizedBox(),
          CustomWidget.customTextFieldWithHeadingWithinContainer(
              context, 'Mobile',
              textEditingController: _controllerPhone,
              keyboardType: TextInputType.phone),
          CustomWidget.getDefaultHeightSizedBox(),
          CustomWidget.getRoundedRaisedButton(context, 'Login',
              textColor: ColorUtils.blackColor, onPressed: () {
            _loginPressed();
          }),
          CustomWidget.getDefaultHeightSizedBox(),
          CustomWidget.getDefaultHeightSizedBox(),
          CustomWidget.getDefaultHeightSizedBox(),
          CustomWidget.getDefaultHeightSizedBox(),
          CustomText.text14Regular(context,
              'You can put any number to login. If user is not registered then app will register that number and then login.')
        ],
      ),
    );
  }

  void _loginPressed() {
    ToastUtils.showToast(context, 'Login Clicked');

    var name = _controllerName.text.toString();
    var phone = _controllerPhone.text.toString();

    if (name.isEmpty) {
      ToastUtils.showToast(context, 'Please Enter User Name');
      return;
    }
    if (phone.isEmpty) {
      ToastUtils.showToast(context, 'Please Enter Phone');
      return;
    }
    //_tempRegisterMultipleUsers();
    AccountProvider.eventLoginUser(context, name, phone);
  }

  void _tempRegisterMultipleUsers() async {
    for (int index = 10; index < 99; index++) {
      var _name = 'Munish $index';
      var _phone = '97119151$index';
      var _randomScore = Random().nextInt(60) + 1;
      var _randomChanceCount = Random().nextInt(10) + 1;

      var _userDetails = UserDetail(
          name: _name,
          phone: _phone,
          score: _randomScore,
          totalChanceCount: _randomChanceCount);
      var _userDetailMap = _userDetails.toJson();
      await FirebaseFirestore.instance
          .collection(ConstantDB.TABLE_USER)
          .doc(_phone)
          .set(_userDetailMap);
    }
  }
}
