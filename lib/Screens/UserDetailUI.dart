import 'dart:async';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myluck/Constants/Constant.dart';
import 'package:myluck/CustomWidgets/CustomText.dart';
import 'package:myluck/CustomWidgets/CustomWidget.dart';
import 'package:myluck/EventBus/EventBusUtils.dart';
import 'package:myluck/EventBus/EventModels/Event3DNavigationDrawerState.dart';
import 'package:myluck/Network/Model/UserDetail.dart';
import 'package:myluck/Provider/AccountProvider.dart';
import 'package:myluck/Provider/Menu3DProvider.dart';
import 'package:myluck/Utils/AppEnums.dart';
import 'package:myluck/Utils/ColorUtils.dart';
import 'package:myluck/Utils/NavigatorUtils.dart';
import 'package:myluck/Utils/PrintUtils.dart';
import 'package:myluck/Utils/ToastUtils.dart';
import '../BaseState.dart';
import '../Utils/AppUtils.dart';

class UserDetailUI extends StatefulWidget {
  @override
  _UserDetailUIState createState() => _UserDetailUIState();
}

class _UserDetailUIState extends State<UserDetailUI> {

  UserDetail _loginResponse;

  @override
  Widget build(BuildContext context) {

    _loginResponse = AccountProvider.getLoginUserInfoWatch(context);

    return Container(
      child: _userRelatedDetails(),
    );
  }

  Widget _userRelatedDetails() {
    return Column(
      children: [
        _getUserRelatedRow('Name', _loginResponse?.name),
        _getUserRelatedRow('Phone', _loginResponse?.phone),
        _getUserRelatedRow('Score', _loginResponse?.score?.toString()),
        _getUserRelatedRow('Chance', _loginResponse?.totalChanceCount?.toString()),
      ],
    );
  }

  Widget _getUserRelatedRow(String title, String value) {
    return Row(
      children: [
        CustomText.text16Regular(context, title ?? ''),
        CustomWidget.getDefaultWidthSizedBox(),
        Expanded(child: CustomText.text16Regular(context, value ?? '')),
      ],
    );
  }
}
