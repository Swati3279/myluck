
class AssetContstants {


  static final String ICON_DICE_1 = "assets/dice_1.png";
  static final String ICON_DICE_2 = "assets/dice_2.png";
  static final String ICON_DICE_3 = "assets/dice_3.png";
  static final String ICON_DICE_4 = "assets/dice_4.png";
  static final String ICON_DICE_5 = "assets/dice_5.png";
  static final String ICON_DICE_6 = "assets/dice_6.png";

  static final String ICON_HEART_NORMAL = "assets/svg/icon_heart_normal.svg";
  static final String ICON_BOOMARK_MENU = "assets/svg/icon_bookmark_menu.svg";
  static final String ICON_SETTINGS = "assets/svg/icon_settings.svg";
  static final String ICON_NOTIFICATION = "assets/svg/icon_notification.svg";

  static final String ICON_LOGOUT = "assets/svg/icon_logout.svg";
}