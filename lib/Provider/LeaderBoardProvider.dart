import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:myluck/Constants/ConstantDB.dart';
import 'package:provider/provider.dart';

class LeaderBoardProvider extends ChangeNotifier {
  List<DocumentSnapshot> listOfUsers;

  Future fetchFirstUserList() async {
    try {
      listOfUsers = (await FirebaseFirestore.instance
              .collection(ConstantDB.TABLE_USER)
              .orderBy("score", descending: true)
              //.limit(100)
              .get())
          .docs;

      //listOfUsers.sort((doc1, doc2) => UserDetail.fromSnapshot(doc1).score.compareTo(UserDetail.fromSnapshot(doc2).score) );

      notifyListeners();
    } catch (e) {
      print(e.toString());
    }
  }

  Future fetchNextUserList() async {
    try {
      List<DocumentSnapshot> newDocumentList = (await FirebaseFirestore.instance
              .collection(ConstantDB.TABLE_USER)
              .orderBy("score")
              .startAfterDocument(listOfUsers[listOfUsers.length - 1])
              .limit(100)
              .get())
          .docs;
      listOfUsers.addAll(newDocumentList);

      notifyListeners();
    } catch (e) {
      print(e.toString());
    }
  }

  static List<DocumentSnapshot> getLeaderBoardUserListingWatch(BuildContext context) => context.watch<LeaderBoardProvider>().listOfUsers;

  static loadInitialData(BuildContext context) =>
      context.read<LeaderBoardProvider>().fetchFirstUserList();

  static loadMoreData(BuildContext context) =>
      context.read<LeaderBoardProvider>().fetchNextUserList();

}
