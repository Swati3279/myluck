import 'package:flutter/material.dart';
import 'package:myluck/EventBus/EventBusUtils.dart';
import 'package:myluck/Utils/AppEnums.dart';
import 'package:provider/provider.dart';

class Menu3DProvider extends ChangeNotifier {
  final TickerProvider vsync;
  AnimationController _animationController;
  EnumMenuState state = EnumMenuState.closed;

  Menu3DProvider({
    this.vsync,
  }) {
    _animationController = new AnimationController(vsync: vsync);
    _animationController
      ..duration = const Duration(milliseconds: 250)
      ..addListener(() {
        notifyListeners();
      })
      ..addStatusListener((AnimationStatus status) {
        switch (status) {
          case AnimationStatus.forward:
            state = EnumMenuState.opening;
            break;
          case AnimationStatus.reverse:
            state = EnumMenuState.closing;
            break;
          case AnimationStatus.completed:
            state = EnumMenuState.open;
            break;
          case AnimationStatus.dismissed:
            state = EnumMenuState.closed;
            break;
        }

        EventBusUtils.eventAppNavigationDrawerState(state);

        notifyListeners();
      });
  }

  @override
  dispose() {
    _animationController.dispose();
    super.dispose();
  }

  get _percentOpen {
    return _animationController.value;
  }

  _open() {
    _animationController.forward();
  }

  _close() {
    _animationController.reverse();
  }

  _toggle() {
    if (state == EnumMenuState.open) {
      _close();
    } else if (state == EnumMenuState.closed) {
      _open();
    }
  }

  static EnumMenuState getMenuStateWatch(BuildContext context) => context.watch<Menu3DProvider>().state;
  static double getPercentageOpenRead(BuildContext context) => context.read<Menu3DProvider>()._percentOpen;
  static double getPercentageOpenSelect(BuildContext context) => context.select((Menu3DProvider _provider) => _provider._percentOpen);
  static void eventOpen(BuildContext context) => context.read<Menu3DProvider>()._open();
  static void eventClose(BuildContext context) => context.read<Menu3DProvider>()._close();
  static void eventToggleRead(BuildContext context) => context.read<Menu3DProvider>()._toggle();
  static void eventToggleSelect(BuildContext context) => context.select((Menu3DProvider _provider) => _provider._toggle());
}
