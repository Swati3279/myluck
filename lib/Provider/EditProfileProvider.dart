import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:myluck/Network/Model/UserDetail.dart';
import 'package:myluck/SharedPref/SharedPrefUtil.dart';
import 'package:myluck/Utils/PrintUtils.dart';
import 'package:provider/provider.dart';

class EditProfileProvider extends ChangeNotifier {

  UserDetail _userDetail;
  UserDetail getUserDetail() => _userDetail;

  void _editProfile(BuildContext context, UserDetail request) async {

  }

  void _getProfileLoggedInUser(BuildContext context) async {

  }

  void _getProfileHitApi(BuildContext context, String _userId) async {
    var userSnapshot = FirebaseFirestore.instance.collection("users").doc(_userId).snapshots();

    userSnapshot.listen((event) {
      final record = UserDetail.fromSnapshot(event);
      PrintUtils.printLog("userSnapshot: ${record.toString()}");
    }, onError: (_e) {
      PrintUtils.printLog("userSnapshot: ERROR");
    });
  }

  void _changeUserDetailData(UserDetail _UserDetail) {
    this._userDetail = _UserDetail;
    notifyListeners();
  }

  /// Static Methods

  static UserDetail watchUserDetailData(BuildContext context,) => context.watch<EditProfileProvider>().getUserDetail();

  static void editProfileRead(BuildContext context, UserDetail request) => context.read<EditProfileProvider>()._editProfile(context, request);
  static void editProfileSelect(BuildContext context, UserDetail request) => context.select((EditProfileProvider _provider) => _provider._editProfile(context, request));

  static void getLoggedInUserDetailRead(BuildContext context) => context.read<EditProfileProvider>()._getProfileLoggedInUser(context);
  static void getLoggedInUserDetailSelect(BuildContext context) => context.select((EditProfileProvider _provider) => _provider._getProfileLoggedInUser(context));

  static void getProfileHitApiRead(BuildContext context, String userId) => context.read<EditProfileProvider>()._getProfileHitApi(context, userId);
  static void getProfileHitApiSelect(BuildContext context, String userId) => context.select((EditProfileProvider _provider) => _provider._getProfileHitApi(context, userId));

}
