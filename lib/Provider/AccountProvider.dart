import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:myluck/Constants/ConstantDB.dart';
import 'package:myluck/Network/Model/UserDetail.dart';
import 'package:myluck/Utils/PrintUtils.dart';
import 'package:myluck/Utils/ToastUtils.dart';
import 'package:provider/provider.dart';

import '../SharedPref/SharedPrefUtil.dart';
import '../Utils/NavigatorUtils.dart';

class AccountProvider extends ChangeNotifier {
  bool _isLogin = false;
  UserDetail _loginResponse;

  bool get isLogin => _isLogin;

  UserDetail get getUserData => _loginResponse;

  AccountProvider() {
    updateAccount();
  }

  void updateAccount() async {
    bool isLogin = await SharedPrefUtil.isLogin();
    setLogin(isLogin);
  }

  void setLogin(bool isLogin) async {
    if (isLogin == null) return;

    this._isLogin = isLogin;
    this._loginResponse = await SharedPrefUtil.getLoginData();
    if(_isLogin && (_loginResponse?.reference == null)) {
      _getUserFromServer(_loginResponse.phone).asStream().listen((event) {

        var _userData = event.data();
        if((_userData?.length ?? 0) > 0) {
          _loginResponse = UserDetail.fromSnapshot(event);
        }
      });

      var userSnapshot = await _getUserFromServer(_loginResponse.phone);

      var _userData = userSnapshot.data();
      if((_userData?.length ?? 0) > 0) {
        _loginResponse = UserDetail.fromSnapshot(userSnapshot);
      }
    }
    notifyListeners();
  }

  void _eventLoginUser(BuildContext context, String name, String phone) async {

    var userSnapshot = await _getUserFromServer(phone);

    var _userData = userSnapshot.data();
    if((_userData?.length ?? 0) > 0) {
      _loginUser(userSnapshot, context);
    } else {
      ToastUtils.showToast(context, 'Trying to register $name with phone $phone');
      _tryToRegisterUser(context, name, phone);
    }

    PrintUtils.printLog('message');
  }

  Future<void> _loginUser(DocumentSnapshot event, BuildContext context) async {
    final record = UserDetail.fromSnapshot(event);
    PrintUtils.printLog("userSnapshot: ${record.toString()}");
    ToastUtils.showToast(context, 'Welcome Back: ${record.name}');
    
    await SharedPrefUtil.saveLoginData(record);
    updateAccount();
  }

  void _tryToRegisterUser(BuildContext context, String name, String phone) async {

    var _userDetails = UserDetail(name: name, phone: phone, score: 0, totalChanceCount: 0);
    var _userDetailMap = _userDetails.toJson();
    await FirebaseFirestore.instance.collection(ConstantDB.TABLE_USER).doc(phone).set(_userDetailMap);

    var userSnapshot = await _getUserFromServer(phone);

    var _userData = userSnapshot.data();
    if((_userData?.length ?? 0) > 0) {
      await _loginUser(userSnapshot, context);
    }
  }

  Future<DocumentSnapshot> _getUserFromServer(String phone) => FirebaseFirestore.instance.collection(ConstantDB.TABLE_USER).doc(phone).get();

  void _updateScoreOfUser(num diceNumber) {
      _loginResponse?.score += diceNumber;
      _loginResponse?.totalChanceCount += 1;
      _loginResponse?.reference?.update(_loginResponse?.toJson());
  }

  void logout(BuildContext context,) async {
    await SharedPrefUtil.logout();
    updateAccount();
    NavigatorUtils.moveToLogin(context);
  }

  static initRead(BuildContext context) =>
      context.read<AccountProvider>().updateAccount();

  static isLoggedInWatch(BuildContext context) =>
      context.watch<AccountProvider>().isLogin;

  static isLoggedInRead(BuildContext context) =>
      context.read<AccountProvider>().isLogin;

  static updateScoreOfUser(BuildContext context, num diceNumber) =>
      context.read<AccountProvider>()._updateScoreOfUser(diceNumber);

  static UserDetail getLoginUserInfoWatch(BuildContext context) =>
      context.watch<AccountProvider>().getUserData;
  
  static void eventLoginUser(BuildContext context, String name, String phone) => context.read<AccountProvider>()._eventLoginUser(context, name, phone);
  
  static logoutRead(BuildContext context) =>
      context.read<AccountProvider>().logout(context);
}
