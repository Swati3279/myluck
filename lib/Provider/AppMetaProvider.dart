import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AppMetaProvider extends ChangeNotifier {

  double _deviceActualWidth = 0.0;
  double _deviceActualHeight = 0.0;

  double _getDeviceWidth() => _deviceActualWidth;
  double _getDeviceHeight() => _deviceActualHeight;

  void _setDeviceWidth(double width) => _deviceActualWidth = width;
  void _setDeviceHeight(double height) => _deviceActualHeight = height;

  static double getDeviceWidth(BuildContext context) => context.watch<AppMetaProvider>()._getDeviceWidth();
  static double getDeviceHeight(BuildContext context) => context.watch<AppMetaProvider>()._getDeviceHeight();

  static void setDeviceWidthRead(BuildContext context, double width) => context.read<AppMetaProvider>()._setDeviceWidth(width);
  static void setDeviceHeightRead(BuildContext context, double height) => context.read<AppMetaProvider>()._setDeviceHeight(height);

  static void setDeviceWidthSelect(BuildContext context, double width) => context.select((AppMetaProvider _provider) => _provider._setDeviceWidth(width));
  static void setDeviceHeightSelect(BuildContext context, double height) => context.select((AppMetaProvider _provider) => _provider._setDeviceHeight(height));
}
