import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:myluck/Utils/AppEnums.dart';
import 'package:provider/provider.dart';

class ConnectivityProvider extends ChangeNotifier {

  ConnectivityStatus _connectivityStatus;

  ConnectivityStatus listenConnectionStatus() => _connectivityStatus;

  bool _isConnected() {
    bool _isConnected = true;
    switch(_connectivityStatus) {

      case ConnectivityStatus.WiFi:
      case ConnectivityStatus.Cellular:
      _isConnected = true;
        break;
      case ConnectivityStatus.Offline:
        _isConnected = false;
        break;
    }

    return _isConnected;
  }

  ConnectivityProvider() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      _connectivityStatus = _getStatusFromResult(result);
      notifyListeners();
    });
  }

  ConnectivityStatus _getStatusFromResult(ConnectivityResult result) {
    switch (result) {
      case ConnectivityResult.mobile:
        return ConnectivityStatus.Cellular;
      case ConnectivityResult.wifi:
        return ConnectivityStatus.WiFi;
      case ConnectivityResult.none:
        return ConnectivityStatus.Offline;
      default:
        return ConnectivityStatus.Offline;
    }
  }

  static bool isConnected(BuildContext context) =>
      context.watch<ConnectivityProvider>()._isConnected();

}
