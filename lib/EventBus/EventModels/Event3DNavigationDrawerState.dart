
import 'package:myluck/Utils/AppEnums.dart';

class Event3DNavigationDrawerState {
  EnumMenuState menuState;

  Event3DNavigationDrawerState({this.menuState});

  void setEvent(EnumMenuState menuState) {
    this.menuState = menuState;
  }
}