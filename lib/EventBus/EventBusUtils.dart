import 'package:event_bus/event_bus.dart';
import 'package:myluck/SharedPref/SharedPrefUtil.dart';
import 'package:myluck/Utils/AppEnums.dart';

import 'EventModels/Event3DNavigationDrawerState.dart';
import 'EventModels/EventLogin.dart';

EventBus eventBus = EventBus();

class EventBusUtils {
  static void eventLogin() async {
    var _isLogin = await SharedPrefUtil.isLogin();
    EventLogin _event = EventLogin(_isLogin);
    eventBus.fire(_event);
  }

  static void eventAppNavigationDrawerState(EnumMenuState menuState) {
    Event3DNavigationDrawerState _event = Event3DNavigationDrawerState(menuState: menuState);
    eventBus.fire(_event);
  }
}
