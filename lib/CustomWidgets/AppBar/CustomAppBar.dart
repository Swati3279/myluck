import 'package:flutter/material.dart';
import 'package:myluck/Constants/Constant.dart';

import '../../Utils/ColorUtils.dart';
import '../CustomText.dart';

class CustomAppBar extends AppBar {

  Widget _getMoveBackIcon(BuildContext context) {
    return IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Theme.of(context).iconTheme.color,
        ),
        onPressed: () {
          Navigator.pop(context);
        });
  }

  Widget _getMoveToDashboardIcon(BuildContext context) {
    return InkWell(
      onTap: () {
        //NavigatorUtils.moveToDashBoard(context);
      },
      child: Container(),
    );
  }

  Widget getAppBar(
      BuildContext context, {
        Key key,
        Widget flexibleSpaceWidget,
        String title = "",
        IconData leadingIcon = Icons.arrow_back_ios,
        double iconSize = 26.0,
        double elevation = 0.0,
        List<Widget> actionWidget,
        bool isBackVisible = true,
      }) {

    if(flexibleSpaceWidget == null) {
      flexibleSpaceWidget = Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Color(0xFFE4E4FF),Color(0xFFF5F4FF)],
            begin: const FractionalOffset(0.0, 0.5),
            end: const FractionalOffset(0.0, 1.0),
            stops: [0.0, 0.9],
          ),
        ),
      );
    }
    return PreferredSize(
      child: AppBar(
        backgroundColor: ColorUtils.redColor,
          flexibleSpace: flexibleSpaceWidget,
          elevation: elevation,
          titleSpacing: 0.0,
          centerTitle: true,
          leading: isBackVisible
              ? _getMoveBackIcon(context)
              : _getMoveToDashboardIcon(context),
          title: CustomText.text20SemiBold(context, title),
          actions: actionWidget),
      preferredSize: Size.fromHeight(Constant.appBarDefaultHeight),
    );
  }
}
