
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:myluck/Utils/ColorUtils.dart';

class CustomText {

  ///
  ///
  ///All Black Bold Texts
  ///
  ///
  static Text text22Bold(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 22.0, int maxLines, TextOverflow textOverflow}) => textBold(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text20Bold(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 20.0,int maxLines, TextOverflow textOverflow}) => textBold(context, text, textSize: textSize,  textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text18Bold(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 18.0,int maxLines, TextOverflow textOverflow}) => textBold(context, text, textSize: textSize,  textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text16Bold(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 16.0,int maxLines, TextOverflow textOverflow}) => textBold(context, text, textSize: textSize,  textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text14Bold(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 14.0,int maxLines, TextOverflow textOverflow}) => textBold(context, text, textSize: textSize,  textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text12Bold(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 12.0,int maxLines, TextOverflow textOverflow}) => textBold(context, text, textSize: textSize,  textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text10Bold(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 10.0,int maxLines, TextOverflow textOverflow}) => textBold(context, text, textSize: textSize,  textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);

  ///
  ///
  ///All Black Semi Bold Texts
  ///
  ///
  static Text text22SemiBold(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 22.0,int maxLines, TextOverflow textOverflow}) => textSemiBold(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text20SemiBold(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 20.0,int maxLines, TextOverflow textOverflow}) => textSemiBold(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text18SemiBold(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 18.0,int maxLines, TextOverflow textOverflow}) => textSemiBold(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text16SemiBold(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 16.0,int maxLines, TextOverflow textOverflow}) => textSemiBold(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text14SemiBold(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 14.0,int maxLines, TextOverflow textOverflow}) => textSemiBold(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text12SemiBold(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 12.0,int maxLines, TextOverflow textOverflow}) => textSemiBold(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text10SemiBold(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 10.0,int maxLines, TextOverflow textOverflow}) => textSemiBold(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);


  ///
  ///
  ///All Black Regular Texts
  ///
  ///
  static Text text22Regular(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 22.0,int maxLines, TextOverflow textOverflow}) => textRegular(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text20Regular(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 20.0,int maxLines, TextOverflow textOverflow}) => textRegular(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text18Regular(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 18.0,int maxLines, TextOverflow textOverflow}) => textRegular(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text16Regular(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 16.0,int maxLines, TextOverflow textOverflow}) => textRegular(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text14Regular(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 14.0,int maxLines, TextOverflow textOverflow}) => textRegular(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text13Regular(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 13.0,int maxLines, TextOverflow textOverflow}) => textRegular(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text12Regular(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 12.0,int maxLines, TextOverflow textOverflow}) => textRegular(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text10Regular(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 10.0,int maxLines, TextOverflow textOverflow}) => textRegular(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);

  ///
  ///
  ///All Grey Regular Texts
  ///
  ///
  static Text text22RegularGrey(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 22.0,int maxLines, TextOverflow textOverflow}) => textRegularGrey(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text20RegularGrey(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 20.0,int maxLines, TextOverflow textOverflow}) => textRegularGrey(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text18RegularGrey(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 18.0,int maxLines, TextOverflow textOverflow}) => textRegularGrey(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text16RegularGrey(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 16.0,int maxLines, TextOverflow textOverflow}) => textRegularGrey(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text14RegularGrey(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 14.0,int maxLines, TextOverflow textOverflow}) => textRegularGrey(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text12RegularGrey(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 12.0,int maxLines, TextOverflow textOverflow}) => textRegularGrey(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text10RegularGrey(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 10.0,int maxLines, TextOverflow textOverflow}) => textRegularGrey(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);


  ///
  ///
  ///All White Bold Texts
  ///
  ///
  static Text text22BoldWhite(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 22.0, int maxLines, TextOverflow textOverflow}) => textBoldWhite(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text20BoldWhite(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 20.0, int maxLines, TextOverflow textOverflow}) => textBoldWhite(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text18BoldWhite(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 18.0, int maxLines, TextOverflow textOverflow}) => textBoldWhite(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text16BoldWhite(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 16.0, int maxLines, TextOverflow textOverflow}) => textBoldWhite(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text14BoldWhite(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 14.0, int maxLines, TextOverflow textOverflow}) => textBoldWhite(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text12BoldWhite(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 12.0, int maxLines, TextOverflow textOverflow}) => textBoldWhite(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text10BoldWhite(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 10.0, int maxLines, TextOverflow textOverflow}) => textBoldWhite(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);


  ///
  ///
  ///All White Regular Texts
  ///
  ///

  static Text text22RegularWhite(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 22.0, int maxLines, TextOverflow textOverflow}) => textRegularWhite(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text20RegularWhite(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 20.0, int maxLines, TextOverflow textOverflow}) => textRegularWhite(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text18RegularWhite(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 18.0, int maxLines, TextOverflow textOverflow}) => textRegularWhite(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text16RegularWhite(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 16.0, int maxLines, TextOverflow textOverflow}) => textRegularWhite(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text14RegularWhite(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 14.0, int maxLines, TextOverflow textOverflow}) => textRegularWhite(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text12RegularWhite(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 12.0, int maxLines, TextOverflow textOverflow}) => textRegularWhite(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  static Text text10RegularWhite(BuildContext context, String text, {TextStyle textStyle, Color textColor, TextAlign textAlign, double textSize = 10.0, int maxLines, TextOverflow textOverflow}) => textRegularWhite(context, text, textSize: textSize, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);




  ///
  /// 
  /// MAIN METHODS
  /// 
  /// 

  static TextStyle getTextStyle(BuildContext context) => Theme.of(context).textTheme.headline1;


  static Text textBold(BuildContext context, String text, {double textSize, TextStyle textStyle, Color textColor, TextAlign textAlign, int maxLines, TextOverflow textOverflow}) {

    if(textAlign == null) {
      textAlign = TextAlign.left;
    }

    return Text(text ?? "", textAlign: textAlign,
        maxLines: maxLines,
        overflow: textOverflow,
        style: textStyle != null ? textStyle : getTextStyle(context).copyWith(fontSize: textSize, fontWeight: FontWeight.w700, color: textColor));
  }

  static Text textSemiBold(BuildContext context, String text, {double textSize, TextStyle textStyle, Color textColor, TextAlign textAlign, int maxLines, TextOverflow textOverflow}) {

    if(textAlign == null) {
      textAlign = TextAlign.left;
    }

    return Text(text ?? "", textAlign: textAlign,
        maxLines: maxLines,
        overflow: textOverflow,
        style: textStyle != null ? textStyle : getTextStyle(context).copyWith(fontSize: textSize, fontWeight: FontWeight.w600, color: textColor));
  }

  static Text textRegular(BuildContext context, String text, {double textSize, TextStyle textStyle, Color textColor, TextAlign textAlign, int maxLines, TextOverflow textOverflow}) {

    if(textAlign == null) {
      textAlign = TextAlign.left;
    }

    return Text(text ?? "",
        textAlign: textAlign,
        maxLines: maxLines,
        overflow: textOverflow,
        style: textStyle != null ? textStyle : getTextStyle(context).copyWith(fontSize: textSize, fontWeight: FontWeight.w400, color: textColor));
  }

  ///
  /// text grey methods
  ///
  static TextStyle getTextStyleGrey(BuildContext context) => Theme.of(context).textTheme.subtitle2;

  static Text textRegularGrey(BuildContext context, String text, {double textSize, TextStyle textStyle, Color textColor, TextAlign textAlign, int maxLines, TextOverflow textOverflow}) {
    if(textColor == null) {
      textColor = ColorUtils.greyTextColor2;
    }

    if(textAlign == null) {
      textAlign = TextAlign.left;
    }

    return Text(text ?? "",
        textAlign: textAlign,
        maxLines: maxLines,
        overflow: textOverflow,
        style: textStyle != null ? textStyle : getTextStyleGrey(context).copyWith(fontSize: textSize, fontWeight: FontWeight.w400, color: textColor));
  }


  /*static Text getText(BuildContext context, String text, {double textSize, FontWeight fontWeight = FontWeight.normal, TextStyle textStyle, Color textColor, TextAlign textAlign}) {
    if(textColor == null) {
      textColor = ColorUtils.headlineTextColor1;
    }

    if(textAlign == null) {
      textAlign = TextAlign.left;
    }

    return Text(text ?? "", textAlign: textAlign,
        overflow: TextOverflow.ellipsis,
        style: textStyle != null ? textStyle : getTextStyle(context).copyWith(fontSize: textSize, fontWeight: fontWeight, color: textColor));
  }*/

  ///
  /// Text Style for White Text
  ///

  static Text textBoldWhite(BuildContext context, String text, {double textSize, TextStyle textStyle, Color textColor, TextAlign textAlign, int maxLines, TextOverflow textOverflow}) {
    return getTextWhite(context, text, textSize: textSize, fontWeight: FontWeight.w700, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  }
  static Text textRegularWhite(BuildContext context, String text, {double textSize, TextStyle textStyle, Color textColor, TextAlign textAlign, int maxLines, TextOverflow textOverflow}) {
    return getTextWhite(context, text, textSize: textSize, fontWeight: FontWeight.w400, textStyle: textStyle, textColor: textColor, textAlign: textAlign, maxLines: maxLines, textOverflow: textOverflow);
  }

  static TextStyle getTextStyleWhite(BuildContext context) => Theme.of(context).textTheme.headline2;

  static Text getTextWhite(BuildContext context, String text, {double textSize, TextStyle textStyle, Color textColor, TextAlign textAlign, TextOverflow textOverflow , int maxLines, FontWeight fontWeight = FontWeight.normal}) {

    if(textColor == null) {
      textColor = ColorUtils.whiteColor;
    }

    if(textAlign == null) {
      textAlign = TextAlign.left;
    }

    return Text(text ?? "", textAlign: textAlign,
        overflow: textOverflow,
        maxLines: maxLines,
        style: textStyle != null ? textStyle : getTextStyleWhite(context).copyWith(fontSize: textSize, fontWeight: fontWeight, color: textColor));
  }

}