import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:myluck/Utils/ColorUtils.dart';

import '../Constants/Constant.dart';
import 'AppBar/CustomAppBar.dart';
import 'CustomText.dart';

class CustomWidget {
  static Widget getCircularUserProfilePic(
    String _userName, {
    double width = 35.0,
    double height = 35.0,
    BuildContext context,
  }) {
    String picUrl = _userName ?? "";

    Widget _circularPicWidget = ClipRRect(
      borderRadius: BorderRadius.circular(10000.0),
      child: Container(
          height: width,
          width: height,
          child: CachedNetworkImage(
            imageUrl: picUrl,
            imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.fill,
                ),
              ),
            ),
            placeholder: (context, url) => getPlaceHolderImageUrl(_userName),
            errorWidget: (context, url, error) =>
                getPlaceHolderImageUrl(_userName),
            placeholderFadeInDuration: Duration(milliseconds: 200),
            fadeInCurve: Curves.fastOutSlowIn,
            fadeInDuration: Duration(milliseconds: 200),
          )),
    );

    if (context != null) {
      return GestureDetector(
          onTap: () {
            /*NavigatorUtils.moveToCommonUserProfileScreen(
                context, _userDetail?.id);*/
          },
          child: _circularPicWidget);
    }

    return _circularPicWidget;
  }

  static Widget getPlaceHolderImageUrl(String _userName,
      {bool isRounded = true, int size, int font_size}) {
    if (size == null) {
      size = 100;
    }

    //int _contentId = Random().nextInt(10);
    int _contentId = 1;
    int hexColorIndex =
        ColorUtils().getColorIndexForTextBackgroundIndex(_contentId);
    String hexColor = ColorUtils()
        .randomColorStringGeneratorForTextBackgroundByIndex(_contentId);
    String textColor = ColorUtils.textColorOnBackgroundInStringLight;

    if (hexColorIndex == 2) {
      textColor = ColorUtils.textColorOnBackgroundInStringDark;
    } else {
      textColor = ColorUtils.textColorOnBackgroundInStringLight;
    }

    String url =
        "https://ui-avatars.com/api/?background=$hexColor&name=$_userName&rounded=$isRounded&color=$textColor&size=$size&font-size=0.42";

    return CachedNetworkImage(
      imageUrl: url,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }

  static Widget getCustomScaffold(
      BuildContext context, String title, Widget child,
      {String badgeCount,
      Widget appBarWidget,
      List<Widget> actionWidget,
      bool isBackVisible = true,
      bool resizeToAvoidBottomPadding = false}) {
    if (resizeToAvoidBottomPadding == null) {
      resizeToAvoidBottomPadding = false;
    }

    if (appBarWidget == null) {
      appBarWidget = CustomAppBar().getAppBar(context,
          title: title,
          actionWidget: actionWidget,
          isBackVisible: isBackVisible);
    }

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomPadding: resizeToAvoidBottomPadding,
        appBar: appBarWidget,
        body: child,
      ),
    );
  }

  static Widget customBackButton(BuildContext context,
      {Function functionToExecute, bool isNeedToFinish = true}) {
    return Container(
      child: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          color: ColorUtils.whiteColor,
          onPressed: () {
            if (functionToExecute != null) {
              functionToExecute();
            }

            if (isNeedToFinish) {
              Navigator.pop(context);
            }
          }),
    );
  }

  static Widget getRoundedRaisedButton(BuildContext context, String text,
      {Function onPressed,
      Color buttonEnableColor,
      Color disableColor = ColorUtils.buttonDisabledBackgroundColor,
      Color disableTextColor = ColorUtils.buttonDisabledTextColor,
      Color textColor = Colors.white,
      double textSize = 16.0,
      double padding = 16.0,
      FontWeight fontWeight = FontWeight.bold}) {
    if (buttonEnableColor == null) {
      buttonEnableColor = ColorUtils.buttonBgColor;
    }

    if (onPressed == null) {
      textColor = ColorUtils.buttonDisabledTextColor;
    }
    return RaisedButton(
      onPressed: onPressed,
      disabledColor: disableColor,
      disabledTextColor: disableTextColor,
      child: CustomText.text16Regular(context, text, textColor: textColor),
      color: buttonEnableColor,
      elevation: 0.0,
      highlightColor: Colors.grey,
      padding: EdgeInsets.only(left: padding * 2, right: padding * 2, top: padding, bottom: padding) ,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(32.0),
      ),
    );
  }

  static Widget customTextFieldWithHeadingWithinContainer_WhiteBackground(
      BuildContext context, String heading,
      {double containerHeight = 40.0,
      FontWeight fontWeight = FontWeight.bold,
      TextEditingController textEditingController,
      String labelText,
      IconData icon,
      Icon suffixIcon,
      Color backgroundColor,
      int maxLines = 1,
      bool isExpand = false,
      onChanged}) {
    var _backgroundColor;
    if (backgroundColor != null) {
      _backgroundColor = backgroundColor;
    } else {
      _backgroundColor = ColorUtils.whiteColor;
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        CustomText.text16Bold(context, heading),
        CustomWidget.getDefaultHeightSizedBox(
            height: Constant.appDefaultSpacingSemiHalf),
        Container(
            height: containerHeight,
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius:
                  BorderRadius.circular(Constant.appDefaultSpacingHalf),
              color: _backgroundColor,
            ),
            child: Padding(
              padding: const EdgeInsets.all(Constant.appDefaultSpacingSemiHalf),
              child: TextFormField(
                  expands: isExpand,
                  maxLines: maxLines,
                  autocorrect: false,
                  minLines: null,
                  style: Theme.of(context)
                      .textTheme
                      .headline1
                      .copyWith(fontSize: 16.0, fontWeight: FontWeight.w400),
                  decoration: new InputDecoration.collapsed(
                    hintText: "",
                  ),
                  controller: textEditingController,
                  onChanged: onChanged),
            )),
      ],
    );
  }

  static Widget customTextFieldWithHeadingWithinContainer(
      BuildContext context, String heading,
      {double containerHeight = 40.0,
      FontWeight fontWeight = FontWeight.bold,
      TextEditingController textEditingController,
      String labelText,
      IconData icon,
      Icon suffixIcon,
      Color backgroundColor,
      int maxLines = 1,
      bool isExpand = false, TextInputType keyboardType: TextInputType.name,
      onChanged}) {
    var _backgroundColor;
    if (backgroundColor != null) {
      _backgroundColor = backgroundColor;
    } else {
      _backgroundColor = ColorUtils.textFieldContainerBgColorLight;
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        CustomText.text16Bold(context, heading),
        CustomWidget.getDefaultHeightSizedBox(
            height: Constant.appDefaultSpacingSemiHalf),
        Container(
            height: containerHeight,
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius:
                  BorderRadius.circular(Constant.appDefaultSpacingHalf),
              color: _backgroundColor,
            ),
            child: Padding(
              padding: const EdgeInsets.all(Constant.appDefaultSpacingSemiHalf),
              child: TextFormField(
                  keyboardType: keyboardType,
                  expands: isExpand,
                  maxLines: maxLines,
                  minLines: null,
                  style: Theme.of(context)
                      .textTheme
                      .headline1
                      .copyWith(fontSize: 16.0, fontWeight: FontWeight.w400),
                  decoration: new InputDecoration.collapsed(
                    hintText: "",
                  ),
                  controller: textEditingController,
                  onChanged: onChanged),
            )),
      ],
    );
  }

  ///
  /// Dividers
  ///

  static Widget getDivider(BuildContext context,
      {double thickness = 0.5, double height = 5.0, Color dividerColor}) {
    if (dividerColor == null) {
      dividerColor = Theme.of(context).dividerColor;
    }
    return Column(
      children: <Widget>[
        Divider(
          height: height,
          thickness: thickness,
          color: dividerColor,
        ),
      ],
    );
  }

  static Widget getDividerNormal({Color dividerColor, double thickness = 1.0}) {
    if (dividerColor == null) {
      dividerColor = ColorUtils.dividerColor;
    }

    return Divider(
      height: 1.0,
      thickness: thickness,
      color: dividerColor,
    );
  }

  static Widget getDefaultWidthSizedBox({double width}) {
    if (width == null) {
      width = Constant.appDefaultSpacing;
    }

    return SizedBox(
      width: width,
    );
  }

  static Widget getDefaultHeightSizedBox({double height}) {
    if (height == null) {
      height = Constant.appDefaultSpacing;
    }

    return SizedBox(
      height: height,
    );
  }
}
