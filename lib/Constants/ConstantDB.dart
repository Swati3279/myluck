import 'package:flutter/widgets.dart';

class ConstantDB {

  ///
  /// All Table Constants
  ///
  static String TABLE_USER = 'users';

  ///
  /// All Docs Constants
  ///
  static String TABLE_DOC = 'temp';

}