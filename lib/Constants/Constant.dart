import 'package:flutter/widgets.dart';

class Constant {

  static double DEFAULT_CARD_WIDTH_LIST = 400.0;
  static final double DEFAULT_CARD_HEIGHT_LIST = 300.0;

  static final double DEFAULT_CARD_HEIGHT_GRID_MAX = 250.0;
  static final double DEFAULT_CARD_HEIGHT_LIST_MAX = 450.0;

  static final double DEFAULT_VIDEO_CARD_HEIGHT_LIST = 250.0;

  static final int API_STATUS_SUCCESS_CODE = 200;

  static const double appBarDefaultHeight = 60.0;

  static double contentCardWidth = 150.0;

  static double deviceActualWidth = 0.0;
  static double deviceActualHeight = 0.0;

  static double contentCardDefaultHeight = 0.0;
  static double contentCardDefaultWidth = 0.0;

  static double contentCardWidthLongHeight = 0.0;
  static double contentCardWidthLongWidth = 0.0;

  static const double appDefaultSpacing = 16.0;
  static const double appDefaultSpacingSemiHalf = 12.0;
  static const double appDefaultSpacingHalf = 8.0;

  static const double cardRadius = 4.0;

  static const double appListSeparatorHeight = 4.0;

  static void initialize(BuildContext context) {
    deviceActualWidth = MediaQuery
        .of(context)
        .size
        .width;
    deviceActualHeight = MediaQuery
        .of(context)
        .size
        .height;

    DEFAULT_CARD_WIDTH_LIST = deviceActualWidth;

    contentCardDefaultWidth = deviceActualWidth / 3;
    contentCardDefaultHeight = contentCardDefaultWidth + 50;

    contentCardWidthLongWidth = deviceActualWidth / 2;
    contentCardWidthLongHeight = contentCardWidthLongWidth / 1.35;
  }
}