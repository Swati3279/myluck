// GENERATED CODE - DO NOT MODIFY BY HAND

part of '../Model/UserDetail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserDetail _$UserDetailFromJson(Map<String, dynamic> json) {
  return UserDetail(
    name: json['name'] as String,
    phone: json['phone'] as String,
    score: json['score'] as num,
    totalChanceCount: json['totalChanceCount'] as num,
  );
}

Map<String, dynamic> _$UserDetailToJson(UserDetail instance) =>
    <String, dynamic>{
      'name': instance.name,
      'phone': instance.phone,
      'score': instance.score,
      'totalChanceCount': instance.totalChanceCount,
    };
