import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'UserDetail.g.dart';

@JsonSerializable(nullable:false)
class UserDetail
{
  String name;
  String phone;
  num score;
  num totalChanceCount;
  DocumentReference reference;

  UserDetail({
    this.name,
    this.phone,
    this.score,
    this.totalChanceCount,
  });

  factory UserDetail.fromJson(Map<String, dynamic> json) =>  _$UserDetailFromJson(json);
  Map<String, dynamic> toJson() => _$UserDetailToJson(this);

  static UserDetail fromSnapshot(DocumentSnapshot snapshot) {
    var _data = snapshot.data();
    var userDetail = UserDetail.fromJson(_data);
    userDetail.reference = snapshot.reference;

    return userDetail;
  }

  @override
  String toString() {
    // TODO: implement toString
    return "$name has Score $score in $totalChanceCount dice rolls";
  }
}


